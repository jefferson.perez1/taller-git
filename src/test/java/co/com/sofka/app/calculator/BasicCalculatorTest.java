package co.com.sofka.app.calculator;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BasicCalculatorTest {
    BasicCalculator basicCalculator;

    @BeforeAll
    public void setUp(){
        basicCalculator = new BasicCalculator();
    }

    @DisplayName("Validando la suma")
    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
            "0,    1,   1",
            "1,    2,   3",
            "49,  51, 100",
            "1,  100, 101"
    })
    public void sumValidation(Long number1,Long number2,Long expectedValue){

        //act
        Long result = basicCalculator.sum(number1,number2);
        // Assert
        Assertions.assertEquals(expectedValue, result);
    }

    @DisplayName("Validando la division")
    @ParameterizedTest(name = "{0} / {1} = {2}")
    @CsvSource({
            "8,    2,   4",
            "10,    2,   5",
            "27,  3, 9",
            "21,  7, 3"
    })
    public void divisionValidar(Long number1,Long number2,Long expectedValue){
        Long result = basicCalculator.div(number1,number2);
        Assertions.assertEquals(expectedValue,result);

    }

    @DisplayName("Validando la division")
    @ParameterizedTest(name = "{0} / {1}")
    @CsvSource({
            "8,    0",
            "5,     0"

    })
    public void divisionValidarExeption(Long number1,Long number2){

        Assertions.assertThrows(IllegalArgumentException.class,()-> {
            basicCalculator.div(number1, number2);
        });

    }
}
